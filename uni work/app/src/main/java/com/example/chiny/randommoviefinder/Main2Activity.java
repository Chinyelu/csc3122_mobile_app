package com.example.chiny.randommoviefinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;

public class Main2Activity extends AppCompatActivity {

    private Button backtofilmbutton;
    private Button clearProfileButton;
    private Button recommendedButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        backtofilmbutton = findViewById(R.id.backToFilms);
        clearProfileButton = findViewById(R.id.clearProfile);
        recommendedButton = findViewById(R.id.recommended);

        backtofilmbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //connects to new activity ie new page
                startActivity(new Intent(Main2Activity.this, MainActivity.class));
            }
        });

        clearProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearProfileMap();
            }
        });
        recommendedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //connects to activity_Main3
                startActivity(new Intent(Main2Activity.this, Main3Activity.class));
            }
        });
    }

    public void clearProfileMap() {
        HashMap<String, Integer> profile = new HashMap<String, Integer>();

        //          <genre, score>
        profile.put("Action", 0);
        profile.put("Adventure", 0);
        profile.put("Animation", 0);
        profile.put("Comedy", 0);
        profile.put("Crime", 0);
        profile.put("Documentary", 0);
        profile.put("Drama", 0);
        profile.put("Family", 0);
        profile.put("Fantasy", 0);
        profile.put("History", 0);
        profile.put("Horror", 0);
        profile.put("Music", 0);
        profile.put("Mystery", 0);
        profile.put("Romance", 0);
        profile.put("Science Fiction", 0);
        profile.put("TV Movie", 0);
        profile.put("Thriller", 0);
        profile.put("War", 0);
        profile.put("Western", 0);


        //saving to userProfile.txt in Device internal storage data/data/com.example...randomMovieFinder/
        //creating a file in phone internal storage with profileMap information
        try {
            FileOutputStream fileout = openFileOutput("userProfile.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(profile.toString());
            outputWriter.close();

            //display file saved message
            Toast.makeText(getBaseContext(), "userProfile.txt Reset!",
                    Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
