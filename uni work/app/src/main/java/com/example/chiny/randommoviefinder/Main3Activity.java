package com.example.chiny.randommoviefinder;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main3Activity extends AppCompatActivity {

    private TextView mostLikedResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

    }

    //attempt to display most liked genres
    public String favoriteGenre() {
        MainActivity m1 = new MainActivity();
        HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
        tempMap = m1.getProfile();

        int maxNum = Collections.max(tempMap.values());

        List<String> keys = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : tempMap.entrySet()) {
            if (entry.getValue() == maxNum) {
                keys.add(entry.getKey());
            }
        }
        String kval = "";

        for (int i = 0; i < keys.size(); i++) {
            kval = kval + keys.get(i);
            Toast.makeText(getBaseContext(), "",
                    Toast.LENGTH_LONG).show();
        }
        mostLikedResult.setText(kval);
        return kval;
    }

}
