package com.example.chiny.randommoviefinder;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

//my imports
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;
import java.util.HashMap;

import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import android.content.Intent;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private TextView textResult;
    private TextView genresResult;
    private WebView webResult;
    private Button notSeenFilm;
    private Button likeFilm;
    private Button dislikeFilm;
    private Button homeButton;

    protected HashMap<String, Integer> profile = new HashMap<String, Integer>(); // profile<genre, score>

    public String fullPosterUrl;
    private String[] currentGenresArray = {"Comedy"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //sets up a new movie when the app loads
        newMovie();

        // gets a new film when clicked
        notSeenFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newMovie();
            }
        });
        likeFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // add the current film genre to the user profile
                userProfileAdd();
                newMovie();
            }
        });
        dislikeFilm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Subtracts the current film genre to the user profile
                userProfileSubtract();
                newMovie();
            }
        });
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(myIntent);
                finish();
            }
        });
    }

    public void newMovie() {
        //initialising the xml layout
        textResult = findViewById(R.id.textViewResult);
        webResult = findViewById(R.id.webViewResult);
        genresResult = findViewById(R.id.genres);
        notSeenFilm = findViewById(R.id.notSeen);
        likeFilm = findViewById(R.id.like);
        dislikeFilm = findViewById(R.id.dislike);
        homeButton = findViewById(R.id.home);

        OkHttpClient client = new OkHttpClient();

        //filmList aFilms = new filmList();
        final String movieTitle = getRandomName();

        if (movieTitle.contains(" ")) {
            movieTitle.replace(" ", "%20");
        }
        String URL = "https://api.themoviedb.org/3/search/movie?api_key=8b3e4cee4754a035bb9ad2a02fd1e7f3&language=en-US&query=" + movieTitle + "&page=1&include_adult=false";

        Request request = new Request.Builder().url(URL).build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final String myResponse = response.body().string();

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //textResult.setText(myResponse);
                            String posterBaseURL = "https://image.tmdb.org/t/p/w185/";

                            String result = myResponse.substring(myResponse.indexOf("poster_path") + 16, myResponse.indexOf(".jpg") + 4);
                            result = posterBaseURL + result;
                            fullPosterUrl = result;
                            textResult.setText(movieTitle);
                            webResult.loadUrl(fullPosterUrl);

                            String genreNums = myResponse.substring(myResponse.indexOf("genre") + 12, myResponse.indexOf("backdrop") - 3);
                            String[] genreArray = genreNums.split(",");
                            currentGenresArray = genreArray;
                            String allNames = "";

                            for (int i = 0; i < genreArray.length; i++) {
                                String genreName = genreMap(Integer.valueOf(genreArray[i]));
                                allNames = allNames + genreName + ",";
                            }
                            if (allNames.endsWith(",")) {
                                allNames = allNames.substring(0, allNames.length() - 1);
                                System.out.println(allNames);
                            }
                            genresResult.setText(allNames);
                        }
                    });
                }
            }
        });
    }

    public String getRandomName() {
        InputStream is = getResources().openRawResource(R.raw.film_text);

        Scanner scan = new Scanner(is);
        String contents = "";

        while (scan.hasNext()) {
            contents = scan.nextLine();
        }

        for (int i = 1900; i <= 2019; i++) {
            String find = "(" + Integer.toString(i) + ")";

            if (contents.contains(find)) {
                contents = contents.replace(find, " ");
            } else if (contents.contains("(3D)")) {
                contents = contents.replace("(3D)", " ");

            } else if (contents.contains("(IMAX)")) {
                contents = contents.replace("(IMAX)", " ");

            } else if (contents.contains("(3D IMAX)")) {
                contents = contents.replace("(3D IMAX)", " ");

            } else if (contents.contains("(4DX)")) {
                contents = contents.replace("(4DX)", " ");
            }
        }
        // converts string into array
        String[] tempArray = contents.split("REMOVETHIS");

        // create a set to eliminate duplicates in the array
        Set<String> mySet = new HashSet<String>(Arrays.asList(tempArray));

        String[] ArrAll = mySet.toArray(new String[mySet.size()]);

        Random rand = new Random();

        int randId = rand.nextInt(tempArray.length) + 0;
        // tempArray.length is the maximum and the 0 is the minimum.

        System.out.println(ArrAll[randId]);

        return ArrAll[randId];

    }

    public String genreMap(int id) {
        HashMap<Integer, String> hm = new HashMap<Integer, String>();

        hm.put(28, "Action");
        hm.put(12, "Adventure");
        hm.put(16, "Animation");
        hm.put(35, "Comedy");
        hm.put(80, "Crime");
        hm.put(99, "Documentary");
        hm.put(18, "Drama");
        hm.put(10751, "Family");
        hm.put(14, "Fantasy");
        hm.put(36, "History");
        hm.put(27, "Horror");
        hm.put(10402, "Music");
        hm.put(9648, "Mystery");
        hm.put(10749, "Romance");
        hm.put(878, "Science Fiction");
        hm.put(10770, "TV Movie");
        hm.put(53, "Thriller");
        hm.put(10752, "War");
        hm.put(37, "Western");

        String outputGenre = hm.get(id);

        return outputGenre;
    }

    public HashMap<String, Integer> myStringToMap(String fromFile) {
        //method converts strings that are in map format eg. {Action=0, Adventure=3} into a map

        HashMap<String, Integer> tempMap = new HashMap<String, Integer>();

        String mapTxtFile = fromFile;
        mapTxtFile = mapTxtFile.replaceAll(" ", "");
        mapTxtFile = mapTxtFile.replace("{", "");
        mapTxtFile = mapTxtFile.replace("}", "");
        String[] slplitCommas = mapTxtFile.split(",");

        for (int i = 0; i < slplitCommas.length; i++) {
            String string = slplitCommas[i];
            System.out.println(string);

            tempMap.put(slplitCommas[i].substring(0, slplitCommas[i].indexOf("=")), Integer
                    .parseInt(slplitCommas[i].substring(slplitCommas[i].indexOf("=") + 1, slplitCommas[i].length())));
        }
        return tempMap;
    }

    public void userProfileAdd() {

        String recoverProfileString = "";
        //read in file
        try {
            FileInputStream filein = openFileInput("userProfile.txt");
            Scanner scan = new Scanner(filein);
            while (scan.hasNext()) {
                recoverProfileString = scan.nextLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //turn string from file into map
        profile = myStringToMap(recoverProfileString);

        //logic to add a 1 to an element if liked and subtract 1 if disliked

        for (int i = 0; i < currentGenresArray.length; i++) {
            int genreScore = profile.get(genreMap(Integer.valueOf(currentGenresArray[i])));
            if (profile.containsKey(genreMap(Integer.valueOf(currentGenresArray[i])))) {
                profile.put(genreMap(Integer.valueOf(currentGenresArray[i])), genreScore + 1);
                Toast.makeText(getBaseContext(), "added " + genreMap(Integer.valueOf(currentGenresArray[i])) + " to score!",
                        Toast.LENGTH_LONG).show();
            }
        }

        //creating a file in phone internal storage with profileMap information
        try {
            FileOutputStream fileout = openFileOutput("userProfile.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(profile.toString());
            outputWriter.close();

            //display file saved message
            Toast.makeText(getBaseContext(), "File saved successfully!",
                    Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void userProfileSubtract() {
        String recoverProfileString = "";

        //read in file
        try {
            FileInputStream filein = openFileInput("userProfile.txt");
            Scanner scan = new Scanner(filein);
            while (scan.hasNext()) {
                recoverProfileString = scan.nextLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //turn string from file into map
        profile = myStringToMap(recoverProfileString);

        //logic to add a 1 to an element if liked and subtract 1 if disliked

        for (int i = 0; i < currentGenresArray.length; i++) {
            int genreScore = profile.get(genreMap(Integer.valueOf(currentGenresArray[i])));
            if (profile.containsKey(genreMap(Integer.valueOf(currentGenresArray[i])))) {
                profile.put(genreMap(Integer.valueOf(currentGenresArray[i])), genreScore - 1);
                Toast.makeText(getBaseContext(), "Subtracted " + genreMap(Integer.valueOf(currentGenresArray[i])) + " from score!",
                        Toast.LENGTH_LONG).show();
            }
        }
        //creating a file in phone internal storage with profileMap information
        try {
            FileOutputStream fileout = openFileOutput("userProfile.txt", MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(profile.toString());
            outputWriter.close();

            //display file saved message
            Toast.makeText(getBaseContext(), "File saved successfully!",
                    Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, Integer> getProfile() {
        return profile;
    }
}
